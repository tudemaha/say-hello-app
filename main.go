package main

import (
	"fmt"

	go_hello_lib "gitlab.com/tudemaha/go-hello-lib/v2"
)

func main() {
	fmt.Println(go_hello_lib.SayHello("Tude Maha"))
}